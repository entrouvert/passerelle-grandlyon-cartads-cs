# -*- coding: utf-8 -*-
# passerelle-grandlyon-cartads-cs
# support for Cart@DS CS connector in Grand Lyon environment
# Copyright (C) 2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import base64
import datetime
import os

from django.core.cache import cache
from django.db import models
from django.utils.translation import ugettext_lazy as _

from passerelle.apps.cartads_cs.models import AbstractCartaDSCS, CartaDSDossier
from passerelle.utils import SOAPTransport
from passerelle.utils.http_authenticators import HttpBearerAuth


class Transport(SOAPTransport):
    def post(self, address, message, headers):
        headers['Authorization'] = self.resource.get_api_manager_token()
        response = super(Transport, self).post(address, message, headers)
        if response.status_code == 401:
            # ask for a new token and retry
            headers['Authorization'] = self.resource.get_api_manager_token(renew=True)
            response = super(Transport, self).post(address, message, headers)
        return response


class GLCartaDSCS(AbstractCartaDSCS):
    category = 'Grand Lyon'
    soap_transport_class = Transport

    token_url = models.URLField(_('Token URL'), max_length=256)
    token_authorization = models.CharField(_('Token Authorization'), max_length=128)
    sendfile_ws_url = models.URLField(_('Sendfile Webservice URL'), max_length=256, blank=True)
    sendfile_ws_dirname = models.CharField(
        _('Sendfile Webservice Directory Name'), max_length=256, blank=True
    )
    verify_cert = models.BooleanField(default=True, verbose_name=_('Check HTTPS Certificate validity'))
    cerfa_pdf_url_substitutions = models.TextField(
        _('URL substitutions for CERFA downloads'),
        help_text=_('On each line, published URL prefix;actual URL prefix'),
        blank=True,
    )

    class Meta:
        verbose_name = 'Cart@DS CS (@ Grand Lyon)'

    def soap_client(self, **kwargs):
        # use original BaseResource soap_client as cart@ds wsdl files cannot be
        # served directly and have to be copied to a different server :/
        return super(AbstractCartaDSCS, self).soap_client(**kwargs)

    def get_cerfa_pdf(self, url):
        url = url.replace('http://ads-rec.grandlyon.fr/', 'https://api-rec.grandlyon.com/ads-rec/')
        url = url.replace('https://ads-rec.grandlyon.fr/', 'https://api-rec.grandlyon.com/ads-rec/')
        url = url.replace('https://ads.grandlyon.fr/', 'https://api.grandlyon.com/ads-pro/')
        if self.cerfa_pdf_url_substitutions:
            for line in self.cerfa_pdf_url_substitutions.splitlines():
                old_prefix, new_prefix = line.strip().split(';')
                url = url.replace(old_prefix, new_prefix)
        return self.requests.get(url, auth=HttpBearerAuth(self.get_api_manager_token()))

    def get_api_manager_token(self, renew=False):
        cache_key = 'cartads-%s-token' % self.id
        if not renew:
            token = cache.get(cache_key)
            if token:
                return token
        headers = {'Authorization': 'Basic %s' % self.token_authorization}
        resp = self.requests.post(
            self.token_url,
            headers=headers,
            data={'grant_type': 'client_credentials'},
            verify=self.verify_cert,
        ).json()
        token = '%s %s' % (resp.get('token_type'), resp.get('access_token'))
        timeout = int(resp.get('expires_in'))
        cache.set(cache_key, token, timeout)
        self.logger.debug('new token: %s (timeout %ss)', token, timeout)
        return token

    def upload_zip(self, zip_filename):
        # But you should really design your site to ensure that the first
        # request to a client-cert-protected area is not a POST request with a
        # large body; make it a GET or something. Any request body has to be
        # buffered into RAM to handle this case, so represents an opportunity
        # to DoS the server.
        # -- https://bz.apache.org/bugzilla/show_bug.cgi?id=39243
        # and that's why there's a seemingly unnecessary GET request first.
        api_manager_token = self.get_api_manager_token()
        response = self.requests.get(self.sendfile_ws_url, headers={'Authorization': api_manager_token})
        if response.status_code == 401:
            api_manager_token = self.get_api_manager_token(renew=True)
        b64_zip = base64.b64encode(open(zip_filename, 'rb').read())
        # call with start=true to initialize file
        resp = self.requests.post(
            self.sendfile_ws_url,
            data={
                'fileName': self.sendfile_ws_dirname + os.path.basename(zip_filename),
                'start': 'true',
            },
        )
        chunk_size = 16777216  # 16MB
        for n in range(0, len(b64_zip), chunk_size):
            resp = self.requests.post(
                self.sendfile_ws_url,
                data={
                    'fileName': self.sendfile_ws_dirname + os.path.basename(zip_filename),
                    'b64_fileContent': b64_zip[n : n + chunk_size].decode('ascii'),
                },
                headers={'Authorization': api_manager_token},
            )
            resp.raise_for_status()
            if resp.content:
                # error messages are put in content, valid responses are empty.
                raise Exception(resp.content)

    def hourly(self):
        client = self.soap_client(wsdl_url=self.get_wsdl_url('ServiceRechercheDossier'))
        client2 = self.soap_client(wsdl_url=self.get_wsdl_url('ServiceDossier'))

        resp = (
            client.service.GetDossiersSelonDerniereEtape(
                self.client_name,
                datetime.datetime.now() - datetime.timedelta(days=1),
                datetime.datetime.now(),
            ),
        )
        if resp and resp[0] is not None:
            for cartads_dossier in resp[0]:
                resp = client2.service.GetInfosDossier(self.get_token(), cartads_dossier['IdDossier'])
                id_dossier_externe = resp['IdDossierExterne']
                if not id_dossier_externe:
                    continue
                if not id_dossier_externe.startswith('publik-'):
                    continue
                publik, dossier_id, tracking_code = id_dossier_externe.split('-', 2)
                try:
                    dossier = CartaDSDossier.objects.get(
                        zip_ready=True,
                        zip_sent=True,
                        zip_ack_response__in=('True', '0'),
                        cartads_id_dossier__isnull=True,
                        tracking_code=tracking_code,
                        id=dossier_id,
                    )
                except CartaDSDossier.DoesNotExist:
                    continue
                dossier.cartads_id_dossier = resp['IdDossier']
                dossier.cartads_numero_dossier = resp['NomDossier']
                dossier.save()
                self.sync_subscribers_role(dossier)

        super(GLCartaDSCS, self).hourly()

    def get_file_status(self, dossier):
        response = super(GLCartaDSCS, self).get_file_status(dossier)
        response['publik_status_label'] = {
            u"En cours de saisie": u"Dossier déposé",
            u"Dossier déposé": u"Dossier déposé",
            u"Dossier transféré au pôle ads": u"Dossier déposé",
            u"Dossier à affecter": u"Dossier déposé",
            u"Complétude à valider": u"Dossier déposé",
            u"Pièces à demander": u"Dossier déposé",
            u"Délais à notifier": u"Dossier déposé",
            u"Attente de pièces": u"Dossier incomplet",
            u"Attente avis de l'instructeur": u"En cours d'instruction",
            u"Attente consultation des services": u"En cours d'instruction",
            u"Attente réponse des services": u"En cours d'instruction",
            u"Attente consultation des services conformité": u"En cours d'instruction",
            u"Attente décision de l'autorité": u"En cours d'instruction",
            u"Réponse des services": u"En cours d'instruction",
            u"Dossier complété": u"En cours d'instruction",
            u"Attente DOC": u"Attente ouverture de chantier",
            u"Attente DAACT": u"Attente achèvement des travaux",
            u"Attente conformité": u"Attente étude conformité",
            u"Attente envoi attestation conformité": u"Attente étude conformité",
            u"En litige": u"Attente étude conformité",
            u"Dossier terminé": u"Dossier terminé",
            u"Prorogation": u"Dossier terminé",
            u"Recours gracieux": u"Dossier terminé",
            u"Dossier transfert": u"Dossier terminé",
            u"Dossier modificatif": u"Dossier terminé",
            u"Contentieux": u"Dossier terminé",
        }.get(response['status_label'], '')
        return response
