# -*- coding: utf-8 -*-


from django.contrib.contenttypes.models import ContentType
from django.core.cache import cache
import django_webtest
import pytest

from grandlyon_cartads_cs.models import GLCartaDSCS
from passerelle.base.models import ApiUser, AccessRight


@pytest.fixture
def app(request):
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    cache.clear()
    yield django_webtest.DjangoTestApp()
    wtm._unpatch_settings()


@pytest.fixture
def connector(db):
    connector = GLCartaDSCS.objects.create(slug='test', token_url='https://whatever')
    api = ApiUser.objects.create(username='all', keytype='', key='')
    obj_type = ContentType.objects.get_for_model(connector)
    AccessRight.objects.create(
        codename='can_access', apiuser=api, resource_type=obj_type, resource_pk=connector.pk
    )


def test_dummny(app, connector):
    assert True
