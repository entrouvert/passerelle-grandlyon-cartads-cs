import os


INSTALLED_APPS += ('grandlyon_cartads_cs',)


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'TEST': {
            'NAME': 'passerelle-grandlyon-cartads-cs-test-%s'
            % os.environ.get("BRANCH_NAME", "").replace('/', '-')[:40],
        },
    }
}
